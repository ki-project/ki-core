# Contributing to Ki

This document contains the information one needs in order to effectively contribute to Ki.
This is a living document and is subject to change at any time. Communication will be
provided in our [Discord](https://discord.gg/jJzpSeB) when this occurs, but it is not 
recommended to rely on such communication and instead periodically check this document.

Last modified: Feb 11, 2020

## Contributions Accepted

We are a community-based project where anyone with the desire to contribute to Ki is
welcomed, regardless of contribution size. Even as basic as fixing a typo in our
documentation is appreciated! That being said, we have a structured process for any type
of contribution and ask that every contribution follows the protocol established in this
document. This will allow simple code review and a smooth Pull Request (PR) process.

## Pull Request Process

### New Issue

- Open an [issue](https://gitlab.com/ki-project/ki-core/issues/new) and describe in the
most detail you can for what problem you're trying to solve, how the problem is replicated,
how you propose to resolve it, and any other details that would be helpful.
- Fork the repo and create a branch off of `develop` named based on the respective issue. 
All work is done off of `develop` as `master` is production-ready code only. If you have
previously forked the repo then please ensure that your fork is up-to-date with 
`ki-project/ki-core`.
- If you would like, you may create a PR early and prepend "WIP" to the PR title. This
communicates with the rest of the team that you are working on it. This is not required
and simply communicating in the issue or developer channels of our Discord is sufficient.
- When you are finished with your contribution either remove the "WIP" from the title or
create the PR from your fork.
- All code must be reviewed by at least one developer so when your PR is ready please
request for a code review in the `#code_review` channel in Discord. If you would like a
specific person or people to review your PR please request them in the PR itself as
Gitlab will automatically notify them.
- We require that each commit be signed (simply a checkbox when committing in Cursive).

### Existing Issue

- Same as new issue, except rather than creating a new issue please simply respond to the
respective issue that you have a proposed solution and willing to implement it. To avoid
potentially duplicated effort, please don't just start working on it without communication.

## Programming Languages, Libraries, & Methodologies

### Programming Languages

- To ensure the [project goals](https://gitlab.com/ki-project/ki-core/-/wikis/home#project-goals)
are simple to attain, [Clojure](https://clojure.org) and [ClojureScript](https://clojurescript.org)
will be used.
- We ask that the [Clojure style guide](https://guide.clojure.style) is followed to 
simplify code reviews and reduce time spent reviewing.
- All PR code will be ran against `cljfmt` (via `lein-cljfmt`) to check for any 
inconsistencies so it is highly recommended to do so yourself when you are about to create
each commit. Instructions on how to do so can be found [here](https://github.com/weavejester/cljfmt).
If you do not do this yourself please be prepared for a reviewer to request any such changes
before the PR is accepted.

### Libraries

- Current libraries used: *TBD*
- We are strongly supportive of careful weighing of trade-offs when proposing a library to
be added. We have a major dependency problem in software projects today and we would like
to be an example of the change we'd like to see.
- As such we would like to only add libraries that are well-documented and have current 
active production use. The library must also provide functionality that is not trivially
implemented as an in-house library or component of existing code.
- We would like to keep our dependency tree as small as possible throughout the project.
Luckily, Clojure's design makes this relatively simple to accomplish!

### Methodologies

- We ask that all code contributions that add or change existing functionality have 
respective specifications created or changed utilizing [clojure.spec](https://clojure.org/about/spec).
This allows a handy capability of automatically creating tests based on the specs rather
than creating tests by hand to shift the responsibility for test correctness to `clojure.spec`
rather than each individual test writer (less points of failure and complexity).
- We ask that development is done with [atomic commits](https://seesparkbox.com/foundry/atomic_commits_with_git)
in mind. As explained in the linked article, atomic commits make project management
significantly more simple. If commits are atomic then rolling back changes is as simple as
performing a git revert rather than having to manually revert changes (prone to error). We 
want this process to be as smooth and consistent as possible. Note: this applies to 
individual commits composing a PR and not to be confused with the merge commit created
upon a successful merge (this is only ever one commit).
- We ask that each function (or similar structure such as a protocol or record) implemented 
has a short docstring explaining what the structure does, regardless of being trivial or 
not to read consistently. If the structure is trivial then the docstring will be very short. 
Please include any requirements that the structure has in order to perform as intended
(program by contract).
- If any contributions affect documentation related to them, we would greatly appreciate
the corresponding documentation changes to be included in the PR.

### Development Environment

- We do not require any particular editor, but highly recommend the use of [Cursive](https://cursive-ide.com).
It supports a paid license if using for commercial purposes, but has a free version of the 
license for Open Source projects, which Ki is!